package com.testpro.EasyUseCase.Core.imp;

import com.testpro.EasyUseCase.Core.Abstract.abstractUiCreate;
import com.testpro.EasyUseCase.Swing.CreateUi;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Administrator testdevops
 * @description 界面化实现用例转化
 * @create 2019-06-13 9:14
 */
public class UIEasyUseCaseImp extends abstractUiCreate {
  @Override
  protected void ExecEasyUseCase() {


    List<String> linkedList = new LinkedList<>();

    if (this.getXMindFilePath() != null) {
      linkedList.add(this.getXMindFilePath());
    }

    if (this.getTargetFileName() != null && this.getTargetFilePath() != null) {
      if (System.getProperty("os.name").equals("Windows")) {
        linkedList.add(this.getTargetFilePath() + "\\" + this.getTargetFileName() + ".xlsx");
      } else {
        linkedList.add(this.getTargetFilePath() + "/" + this.getTargetFileName() + ".xlsx");
      }
    }

    if (this.getTestNo() != null) {
      linkedList.add(this.getTestNo());
    }
    XMindUseCase convert = new XMindUseCase();
    if (linkedList.size() == 3) {
      convert.setInputXMindFilePath(linkedList.get(0));
      convert.setOutPutExcelFilePath(linkedList.get(1));
      System.setProperty(CreateUi.SYSTEM_TEST_NO, linkedList.get(2));
    }
    if (linkedList.size() == 2) {
      convert.setInputXMindFilePath(linkedList.get(0));
      convert.setOutPutExcelFilePath(linkedList.get(1));
      System.clearProperty(CreateUi.SYSTEM_TEST_NO);
    }
    if (linkedList.size() == 1) {
      convert.setInputXMindFilePath(linkedList.get(0));
    }
    convert.Convert();
  }
}
