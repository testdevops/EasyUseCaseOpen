package com.testpro.EasyUseCase.Core.Abstract;

import com.testpro.EasyUseCase.Swing.CreateUi;
import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * 抽象UI实现 @Author Administrator testers @Create 2019-06-12 22:05
 */
@Getter
@Setter
public abstract class abstractUiCreate implements CreateUi, ActionListener {

    private String XMindFilePath;
    private String targetFilePath;
    private String targetFileName;
    private String testNo;
    JFrame frame = new JFrame("EasyUseCase1.4.3 官方唯一QQ群31043004"); // 框架布局
    static Container con = new Container();
    static JLabel label1 = new JLabel("Xmind文件");
    static JLabel label2 = new JLabel("Excel目录");
    static JLabel label3 = new JLabel("测试用例编号");
    static JLabel label4 = new JLabel("Excel名称");
    static JCheckBox checkBox = new JCheckBox("大纲");
    static JTextField XMindFilePathJText = new JTextField();
    static JTextField targetFilePathJText = new JTextField();
    static JTextField targetFileNameJText = new JTextField();
    static JTextField testNoJText = new JTextField();
    static JButton button1 = new JButton("..."); // 选择
    static JButton button2 = new JButton("..."); // 选择
    static JButton button3 = new JButton("确定"); //
    static JFileChooser jfc = new JFileChooser(); // 文件选择器

    {
        XMindFilePathJText.setEditable(false);
        label1.setBounds(10, 10, 70, 20);

        XMindFilePathJText.setBounds(105, 10, 250, 20);
        XMindFilePathJText.setText("E:\\input.xmind");
        button1.setBounds(355, 10, 50, 20);

        label2.setBounds(10, 35, 70, 20);
        targetFilePathJText.setBounds(105, 35, 250, 20);
        targetFilePathJText.setText("C:");

        button2.setBounds(355, 35, 50, 20);
        label3.setBounds(10, 55, 95, 20);
        testNoJText.setBounds(105, 55, 120, 20);
        button3.setBounds(50, 115, 60, 20);

        label4.setBounds(10, 75, 95, 20);
        targetFileNameJText.setBounds(105, 75, 120, 20);
        checkBox.setBounds(new Rectangle(98, 95, 95, 20));
        button1.addActionListener(this); // 添加事件处理
        button2.addActionListener(this); // 添加事件处理
        button3.addActionListener(this); // 添加事件处理
        checkBox.addActionListener(this);
    }

    @Override
    public void CreateUIConfig() {

        CreateUI();
    }

    protected abstract void ExecEasyUseCase();

    private void CreateUI() {

        double lx = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double ly = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        frame.setLocation(new Point((int) (lx / 2) - 150, (int) (ly / 2) - 150)); // 设定窗口出现位置
        frame.setSize(500, 200); // 设定窗口大小
        frame.setVisible(true); // 窗口可见
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // 使能关闭窗口，结束程序
        frame.add(label1);
        frame.add(label2);
        frame.add(label3);
        frame.add(label4);
        frame.add(XMindFilePathJText);
        frame.add(targetFilePathJText);
        frame.add(targetFileNameJText);
        frame.add(testNoJText);
        frame.add(button1);
        frame.add(button2);
        frame.add(button3);
        frame.add(checkBox);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JCheckBox) {
            final JCheckBox source = (JCheckBox) e.getSource();
            if (source.isSelected()) {
                System.setProperty("DG", "true");
            } else System.setProperty("DG", "false");
        }

        if (e.getSource().equals(button1)) { // 判断触发方法的按钮是哪个
            jfc.setFileSelectionMode(JFileChooser.FILES_ONLY); // 设定只能选择到文件
            int state = jfc.showOpenDialog(null); // 此句是打开文件选择器界面的触发语句
            if (state == 1) {
                return;
            } else {
                File f = jfc.getSelectedFile(); // f为选择到的目录
                XMindFilePathJText.setText(f.getAbsolutePath());
                this.setXMindFilePath(f.getAbsolutePath());
            }
        }
        // 绑定到选择文件，先择文件事件
        if (e.getSource().equals(button2)) {
            jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); // 设定只能选择到文件夹
            int state = jfc.showOpenDialog(null); // 此句是打开文件选择器界面的触发语句
            if (state == 1) {
                return; // 撤销则返回
            } else {
                File f = jfc.getSelectedFile(); // f为选择到的文件
                targetFilePathJText.setText(f.getAbsolutePath());
                this.setTargetFilePath(f.getAbsolutePath());
            }
        }
        if (e.getSource().equals(button3)) {
            this.setXMindFilePath(XMindFilePathJText.getText());
            this.setTargetFilePath(targetFilePathJText.getText());
            if (this.getXMindFilePath() != null && this.getTargetFilePath() == null) {

                JOptionPane.showMessageDialog(null, "请选择目标Excel文件路径！", "提示", JOptionPane.WARNING_MESSAGE);

                return;
            }
            if (this.getXMindFilePath() != null && this.getXMindFilePath() != null && targetFileNameJText.getText() == null) {

                JOptionPane.showMessageDialog(null, "请填写文件名称无需包含xlsx！", "提示", JOptionPane.WARNING_MESSAGE);
                return;
            }
            if (this.getXMindFilePath() != null && this.getXMindFilePath() != null && !targetFileNameJText.getText().equals("") && !testNoJText.getText().equals("")) {
                this.setTargetFileName(targetFileNameJText.getText());
                this.setTestNo(testNoJText.getText());
                System.out.println("有编号模式");
                ExecEasyUseCase();
                return;
            }

            if (this.getXMindFilePath() != null && this.getXMindFilePath() != null && !targetFileNameJText.getText().equals("")) {
                this.setTargetFileName(targetFileNameJText.getText());
                this.setTestNo(null); // 解决无编号模式中变量未重置问题
                System.out.println("无编号模式");
                ExecEasyUseCase();
                return;
            }
            JOptionPane.showMessageDialog(null, "请正确填写相关参数！", "提示", JOptionPane.WARNING_MESSAGE);
        }
    }
}
