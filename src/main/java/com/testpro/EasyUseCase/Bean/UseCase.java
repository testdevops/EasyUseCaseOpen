package com.testpro.EasyUseCase.Bean;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UseCase extends BaseRowModel {

  @ExcelProperty(index = 0, value = "序号")
  private String setups;

  @ExcelProperty(index = 1, value = "模块")
  private String modular;

  @ExcelProperty(index = 2, value = "测试项名称")
  private String caseName;

  @ExcelProperty(index = 3, value = "优先级")
  private String marker;

  @ExcelProperty(index = 4, value = "测试用例编号")
  private String caseNo;

  @ExcelProperty(index = 5, value = "测试目的")
  private String caseObjective;

  @ExcelProperty(index = 6, value = "前置条件")
  private String precondition;

  @ExcelProperty(index = 7, value = "操作步骤")
  private String steps;

  @ExcelProperty(index = 8, value = "预期结果")
  private String expectedResults;

  @ExcelProperty(index = 9, value = "实际测试结果")
  private String checkResult;

  @ExcelProperty(index = 10, value = "备注信息")
  private String reMarks;
}
