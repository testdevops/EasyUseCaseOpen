package com.testpro.EasyUseCase.Bean;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/** @author Administrator testdevops @Date 2020-06-05 15:12 */
@Getter
@Setter
@ToString
public class UseCaseDG extends BaseRowModel {
  @ExcelProperty(index = 0, value = "大模块")
  private String bigModular;

  @ExcelProperty(index = 1, value = "小模块")
  private String modular;

  @ExcelProperty(index = 2, value = "功能点")
  private String testName;

  @ExcelProperty(index = 3, value = "预期结果")
  private String expectedResults;
}
